import axios from "axios";

export default async function createdAutomation(automate, start, end) {
  if (automate === "pickup") {
    const pickup = await axios.post(process.env.REACT_APP_API_URL, {
      gripper: Number(start.gripper),
      w: Number(start.w),
      x: Number(start.x),
      y: Number(start.y),
      z: Number(start.z),
      base: Number(start.base),
    });
    if (pickup.status === 200) {
      axios.post(process.env.REACT_APP_API_URL, {
        gripper: 1000,
        w: Number(start.w),
        x: Number(start.x),
        y: 500,
        z: 500,
        base: Number(start.base),
      });
    }
  }
  if (automate === "drop") {
    const dropoff = await axios.post(process.env.REACT_APP_API_URL, {
      gripper: 1000,
      w: Number(end.w),
      x: Number(end.x),
      y: Number(end.y),
      z: Number(end.z),
      base: Number(end.base),
    });
    if (dropoff.status === 200) {
      axios.post(process.env.REACT_APP_API_URL, {
        gripper: Number(end.gripper),
        w: 500,
        x: 500,
        y: 500,
        z: 500,
        base: Number(start.base),
      });
    }
  }
}
