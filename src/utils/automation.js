import axios from "axios";

export default async function automation(automate) {
  if (automate === "pickup") {
    const pickup = await axios.post(process.env.REACT_APP_API_URL, {
      gripper: 250,
      w: 500,
      x: 500,
      y: 725,
      z: 665,
      base: 105,
    });
    if (pickup.status === 200) {
      axios.post(process.env.REACT_APP_API_URL, {
        gripper: 535,
        w: 500,
        x: 500,
        y: 725,
        z: 390,
        base: 105,
      });
    }
  }
  if (automate === "drop") {
    const dropoff = await axios.post(process.env.REACT_APP_API_URL, {
      gripper: 535,
      w: 500,
      x: 500,
      y: 665,
      z: 605,
      base: 500,
    });
    if (dropoff.status === 200) {
      axios.post(process.env.REACT_APP_API_URL, {
        gripper: 250,
        w: 500,
        x: 500,
        y: 500,
        z: 500,
        base: 500,
      });
    }
  }
}
