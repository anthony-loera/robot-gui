import "./App.css";
import Appbar from "./components/Header";
import Controller from "./components/Controller";
import AutomationCard from "./components/AutomationCard";
import AutomationCalibration from "./components/AutomationCalibration";
import { Grid } from "@mui/material";

function App() {
  return (
    <div className="App">
      <Appbar />
      <div style={{ padding: "12px" }}>
        <Grid container>
          <Grid item sm={12} md={12} l={8} xl={8}>
            <Controller />
          </Grid>
          <Grid item sm={12} md={12} l={4} xl={4}>
            <AutomationCalibration />
            <AutomationCard />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

export default App;
