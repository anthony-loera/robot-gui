import { Button } from "@mui/material";
import axios from "axios";
import { useEffect, useRef, useState } from "react";
import automation from "../utils/automation";
import { Default_Values } from "./Controller";

export default function AutomationCard() {
  const [position, setPosition] = useState(Default_Values);
  const [automate, setAutomate] = useState(null);
  const timeoutId = useRef();

  const handleReset = () => {
    setAutomate(null);
    clearTimeout(timeoutId.current);
    setPosition(Default_Values);
    axios.post(process.env.REACT_APP_API_URL, { ...position });
  };

  const handleAutomation = () => {
    setAutomate("pickup");
  };

  useEffect(() => {
    timeoutId.current = setTimeout(() => {
      if (automate === "pickup") {
        automation(automate);
        setAutomate("drop");
      }
      if (automate === "drop") {
        automation(automate);
        setAutomate("pickup");
      }
    }, 5000);
  }, [automate]);

  return (
    <>
      {!automate && (
        <Button onClick={handleAutomation}>Start Demo Automation</Button>
      )}
      {automate && <Button onClick={handleReset}>Stop Demo Automation</Button>}
    </>
  );
}
