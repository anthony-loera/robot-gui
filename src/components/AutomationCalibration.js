import {
  Button,
  Card,
  CardContent,
  Container,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useEffect, useRef, useState } from "react";
import createdAutomation from "../utils/createdAutomation";
import { Default_Values } from "./Controller";

export default function SetnewAutomation() {
  const [startPosition, setStartPosition] = useState(Default_Values);
  const [endPosition, setEndPosition] = useState(Default_Values);
  const [automate, setAutomate] = useState(null);
  const timeoutId = useRef();

  const handleStartChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setStartPosition({
      ...startPosition,
      [name]: value,
    });
  };

  const handleEndChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setEndPosition({
      ...endPosition,
      [name]: value,
    });
  };

  const handleReset = () => {
    setAutomate(null);
    clearTimeout(timeoutId.current);
    setStartPosition(Default_Values);
    setEndPosition(Default_Values);
    axios.post(process.env.REACT_APP_API_URL, { ...startPosition });
  };

  const handleAutomation = () => {
    setAutomate("pickup");
  };

  useEffect(() => {
    timeoutId.current = setTimeout(() => {
      if (automate === "pickup") {
        createdAutomation(automate, startPosition, endPosition);
        setAutomate("drop");
      }
      if (automate === "drop") {
        createdAutomation(automate, startPosition, endPosition);
        setAutomate("pickup");
      }
    }, 5000);
  }, [automate, startPosition, endPosition]);

  return (
    <Container maxWidth="sm">
      <Card
        sx={{
          fontFamily: "Georgia, serif",
          marginTop: "20px",
          marginLeft: "auto",
          marginRight: "auto",
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-evenly",
          color: "#000",
          padding: "2px",
          textAlign: "center",
        }}
      >
        <Typography variant="h3" component="div">
          Set New Automation
        </Typography>
        <Typography variant="h6" component="div">
          Range 0 - 1000
        </Typography>
        <CardContent>
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Gripper Start"
            name="gripper"
            value={startPosition.gripper}
            variant="outlined"
            onChange={handleStartChange}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Gripper End"
            name="gripper"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.gripper}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="W Start"
            name="w"
            variant="outlined"
            onChange={handleStartChange}
            value={startPosition.w}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="W End"
            name="w"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.w}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="X Start"
            name="x"
            variant="outlined"
            onChange={handleStartChange}
            value={startPosition.x}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            label="X End"
            name="x"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.x}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Y Start"
            name="y"
            variant="outlined"
            onChange={handleStartChange}
            value={startPosition.y}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Y End"
            name="y"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.y}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Z Start"
            name="z"
            variant="outlined"
            onChange={handleStartChange}
            value={startPosition.z}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Z End"
            name="z"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.z}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Base Start"
            name="base"
            variant="outlined"
            onChange={handleStartChange}
            value={startPosition.base}
          />
          <TextField
            sx={{ padding: "2px" }}
            required
            type="number"
            label="Base End"
            name="base"
            variant="outlined"
            onChange={handleEndChange}
            value={endPosition.base}
          />
        </CardContent>
        {!automate && (
          <Button variant="contained" onClick={handleAutomation}>
            Start New Automation
          </Button>
        )}
        {automate && (
          <Button variant="contained" onClick={handleReset}>
            Stop New Automation
          </Button>
        )}
        <Button onClick={handleReset}>Reset</Button>
      </Card>
    </Container>
  );
}
