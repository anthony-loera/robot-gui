import {
  Button,
  Card,
  CardContent,
  Container,
  Slider,
  Typography,
} from "@mui/material";
import axios from "axios";
import { useState } from "react";

export const Default_Values = {
  gripper: 500,
  w: 500,
  x: 500,
  y: 500,
  z: 500,
  base: 500,
};

const marks = [
  {
    value: 0,
    label: "0",
  },
  {
    value: 250,
    label: "250",
  },
  {
    value: 500,
    label: "500",
  },
  {
    value: 750,
    label: "750",
  },
  {
    value: 1000,
    label: "1000",
  },
];

export default function Controller() {
  const [position, setPosition] = useState(Default_Values);

  const handleChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;
    setPosition({
      ...position,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    axios.post(process.env.REACT_APP_API_URL, { ...position });
  };
  const handleReset = () => {
    setPosition(Default_Values);
    axios.post(process.env.REACT_APP_API_URL, { ...position });
  };

  return (
    <Container maxWidth="sm">
      <Card
        sx={{
          fontFamily: "Georgia, serif",
          marginTop: "20px",
          marginLeft: "20px",
          marginRight: "10px",

          display: "flex",
          flexDirection: "column",
          justifyContent: "space-evenly",
          color: "#000",
          padding: "1vh",
          textAlign: "center",
        }}
      >
        <Typography variant="h3" component="div">
          Controller
        </Typography>
        <CardContent>
          <Typography id="input-slider" sx={{ paddingBottom: "3vh" }}>
            Gripper
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.gripper}
            step={5}
            max={1000}
            min={0}
            name="gripper"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
          <Typography id="input-slider" m sx={{ paddingBottom: "3vh" }}>
            W
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.w}
            step={5}
            max={1000}
            min={0}
            name="w"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
          <Typography id="input-slider" sx={{ paddingBottom: "3vh" }}>
            X
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.x}
            step={5}
            max={1000}
            min={0}
            name="x"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
          <Typography id="input-slider" sx={{ paddingBottom: "3vh" }}>
            Y Axis
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.y}
            step={5}
            max={1000}
            min={0}
            name="y"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
          <Typography id="input-slider" sx={{ paddingBottom: "3vh" }}>
            Z
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.z}
            step={5}
            max={1000}
            min={0}
            name="z"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
          <Typography id="input-slider" sx={{ paddingBottom: "3vh" }}>
            Base
          </Typography>
          <Slider
            aria-label="Always visible"
            value={position.base}
            step={5}
            max={1000}
            min={0}
            name="base"
            onChangeCommitted={handleSubmit}
            onChange={handleChange}
            marks={marks}
            valueLabelDisplay="on"
          />
        </CardContent>
        <Button onClick={handleReset}>Reset</Button>
      </Card>
    </Container>
  );
}
